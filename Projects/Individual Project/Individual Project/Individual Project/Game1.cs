using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace Individual_Project
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;             
        SpriteBatch spriteBatch;

        //My global constants
        const int SCREEN_WIDTH = 1200;            //dimensions of the screen
        const int SCREEN_HEIGHT = 900;
        const int SPEED = 1;                     //a general variable for speed, for thing such as background transitions
        const string GAME_OVER_PROMPT = "GAME OVER";
        const string GAME_OVER_NAVIGATION = "PRESS C TO GO BACK";          //prompts
        const string ROUND_COMPLETED_PROMPT = "ROUND COMPLETE";
        const int HEALTHICON_WIDTH = 100;
        const int HEALTHICON_HEIGHT = 100;

        //My global variables
        TGameState gameState = TGameState.splashScreen;
        KeyboardState OldKeys, MyKeys;
        enum TGameState { splashScreen, Rules, playGame, gameOver, Revision }

        Texture2D TBackground, TBackground2, TsplashScreen, TRules, TPeriodic, TSpacestation, TShot, SpriteTexture, THealth, RussianPeriodicTable, ChinesePeriodicTable, JapanesePeriodicTable; //General textures

        Texture2D actinium, americium, berkelium, californium, 
            curium, einsteinium, fermium, mendelevium, neptunium, nobelium, plutonium, 
            protactinium, thorium, uranium, cesium, francium, lithium, potassium, rubidium, sodium, 
            barium, beryllium, calcium, magnesium, radium, strontium, astatine, bromine, chlorine, fluorine, 
            iodine, cerium, dysprosium, erbium, europium, gadolinium, holmium, lanthanum, neodymium, praseodymium, promethium, 
            samarium, terbium, thulium, ytterbium, antimony, arsenic, boron, germanium, silicon, tellurium, argon, helium, krypton, neon, 
            radon, xenon, carbon, hydrogen, nitrogen, oxygen, phosphorus, selenium, sulfur, aluminium, bismuth, gallium, indium, lead, polonium, thallium, 
            tin, bohrium, cadmium, chromium, cobalt, copper, dubnium, gold, hafnium, hassium, iridium, iron, lawrencium, lutetium, manganese, meitnerium, mercury, molybdenum, 
            nickel, niobium, osmium, palladium, platinum, rhenium, rhodium, ruthemium, rutherfordium, scandium, seaborgium, silver, tantalum, technetium, titanium, tungsten, ununnilium, //Specifically element textures
            unununium, vanadium, yttrium, zinc, zirconium;

        Rectangle Background, Background2, MenuScreen, Spacestation, HealthIcon, PeriodicTable;
        SpriteFont FontGameOver, FontGameOverNavigation, DisplayedWordFont, TypeCheckError, LanguageFont;

        SoundEffect CActinium, CAmericium, CBerkelium, CCalifornium, CCurium, CEinsteinium, CFermium, CMendelevium, CNeptunium, CNobelium, CPlutonium, CProtactinium, CThorium, CUranium,
            JActinium, JAmericium, JBerkelium, JCalifornium, JCurium, JEinsteinium, JFermium, JMendelevium, JNeptunium, JNobelium, JPlutonium, JProtactinium, JThorium, JUranium,
            RActinium, RAmericium, RBerkelium, RCalifornium, RCurium, REinsteinium, RFermium, RMendelevium, RNeptunium, RNobelium, RPlutonium, RProtactinium, RThorium, RUranium; //All the sound effect for the elements
        
        int Word1TexturePositionX, Word1TexturePositionY, Word2TexturePositionX, Word2TexturePositionY; //the positioning of the strings (words) undernieth the spaceships
        int LetterPosition; //the index of the character's position within a string

        string AssistLanguageChosen = "None"; //Language choice
        bool SoundPlayed = false;
        double Score; //the total score
        bool Victory;
        
        object items;
        int positionpointer; //Position of our current character target in the word
        bool Gameover;
        string ElementChoice = null;

        bool LockedWord = false; //A locked word is the word we're currently locked on trying to spell out
        string OldWord;

        bool isTimerOn = false;
        float counter = 10; // 10 seconds

        SpaceStationControl PlayerStation = new SpaceStationControl();
        TDynamicQueue Enemies;
        TEnemy Enemy = new TEnemy(1, "Hello");
        TShot Shot = new TShot();

        xButton btnPlay, btnActinides, btnAlkalaiMetals, btnAlkalineEarthMetals, btnHalogens, btnLanthanides, btnMetalloids, btnNobleGases, btnNonMetals, btnPostTransitionMetals, btnTransitionMetals;

        GameTime gameTime;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 1200;
            graphics.PreferredBackBufferHeight = 900;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();

            // TODO: Add your initialization logic here
            Score = 0;
            Background = new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            Background2 = new Rectangle(0, 0 - SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT);
            Spacestation = new Rectangle(373, SCREEN_HEIGHT - 310, 300, 200);
            MenuScreen = new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            Gameover = false;
            Victory = false;
            HealthIcon = new Rectangle(0, 0, HEALTHICON_WIDTH, HEALTHICON_HEIGHT);
            Enemies = new TDynamicQueue(ElementChoice);
            PeriodicTable = new Rectangle(150, 300, 800, 550);
            gameTime = new GameTime();
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            TBackground = Content.Load<Texture2D>("Background1");
            TBackground2 = Content.Load<Texture2D>("Background2");
            TsplashScreen = Content.Load<Texture2D>("Menu");
            TRules = Content.Load<Texture2D>("Rules");
            TPeriodic = Content.Load<Texture2D>("Periodic");
            TSpacestation = Content.Load<Texture2D>("Book");
            SpriteTexture = Content.Load<Texture2D>("Gun");
            TShot = Content.Load<Texture2D>("Bullet");
            PlayerStation.spritePosition = new Vector2(520, 630);
            RussianPeriodicTable = Content.Load<Texture2D>("RussianTable");
            ChinesePeriodicTable = Content.Load<Texture2D>("ChineseTable");
            JapanesePeriodicTable = Content.Load<Texture2D>("JapaneseTable");

            actinium = Content.Load<Texture2D>("Actinium");
            americium = Content.Load<Texture2D>("Americium");
            berkelium = Content.Load<Texture2D>("Berkelium");
            californium = Content.Load<Texture2D>("Californium");
            curium = Content.Load<Texture2D>("Curium");
            einsteinium = Content.Load<Texture2D>("Einsteinium");
            fermium = Content.Load<Texture2D>("Fermium");
            mendelevium = Content.Load<Texture2D>("Mendelevium");
            neptunium = Content.Load<Texture2D>("Neptunium");
            nobelium = Content.Load<Texture2D>("Nobelium");
            plutonium = Content.Load<Texture2D>("Plutonium");
            protactinium = Content.Load<Texture2D>("Protactinium");
            thorium = Content.Load<Texture2D>("Thorium");
            uranium = Content.Load<Texture2D>("Uranium");
 
            astatine = Content.Load<Texture2D>("Astatine");
            bromine = Content.Load<Texture2D>("Bromine");
            chlorine = Content.Load<Texture2D>("Chlorine");
            fluorine = Content.Load<Texture2D>("Fluorine");
            iodine = Content.Load<Texture2D>("Iodine");

            cerium = Content.Load<Texture2D>("Cerium");
            dysprosium = Content.Load<Texture2D>("Dysprosium");
            erbium = Content.Load<Texture2D>("Erbium");
            europium = Content.Load<Texture2D>("Europium");
            gadolinium = Content.Load<Texture2D>("Gadolinium");
            holmium = Content.Load<Texture2D>("Holmium");
            lanthanum = Content.Load<Texture2D>("Lanthanum");
            neodymium = Content.Load<Texture2D>("Neodymium");
            praseodymium = Content.Load<Texture2D>("Praseodymium");
            promethium = Content.Load<Texture2D>("Promethium");
            samarium = Content.Load<Texture2D>("Samarium");
            terbium = Content.Load<Texture2D>("Terbium");
            thulium = Content.Load<Texture2D>("Thulium");
            ytterbium = Content.Load<Texture2D>("Ytterbium");

            antimony = Content.Load<Texture2D>("Antimony");
            arsenic = Content.Load<Texture2D>("Arsenic");
            boron = Content.Load<Texture2D>("Boron");
            germanium = Content.Load<Texture2D>("Germanium");
            silicon = Content.Load<Texture2D>("Silicon");
            tellurium = Content.Load<Texture2D>("Tellurium");

            argon = Content.Load<Texture2D>("Argon");
            helium = Content.Load<Texture2D>("Helium");
            krypton = Content.Load<Texture2D>("Krypton");
            neon = Content.Load<Texture2D>("Neon");
            radon = Content.Load<Texture2D>("Radon");
            xenon = Content.Load<Texture2D>("Xenon");

            carbon = Content.Load<Texture2D>("Carbon");
            hydrogen = Content.Load<Texture2D>("Hydrogen");
            nitrogen = Content.Load<Texture2D>("Nitrogen");
            oxygen = Content.Load<Texture2D>("Oxygen");
            phosphorus = Content.Load<Texture2D>("Phosphorus");
            selenium = Content.Load<Texture2D>("Selenium");
            sulfur = Content.Load<Texture2D>("Sulfur");
            
            aluminium = Content.Load<Texture2D>("Aluminium");
            bismuth = Content.Load<Texture2D>("Bismuth");
            gallium = Content.Load<Texture2D>("Gallium");
            indium = Content.Load<Texture2D>("Indium");
            lead = Content.Load<Texture2D>("Lead");
            polonium = Content.Load<Texture2D>("Polonium");
            thallium = Content.Load<Texture2D>("Thallium");
            tin = Content.Load<Texture2D>("Tin");

            bohrium = Content.Load<Texture2D>("Bohrium");
            cadmium = Content.Load<Texture2D>("Cadmium");
            chromium = Content.Load<Texture2D>("Chromium");
            cobalt = Content.Load<Texture2D>("Cobalt");
            copper = Content.Load<Texture2D>("Copper");
            dubnium = Content.Load<Texture2D>("Dubnium");
            gold = Content.Load<Texture2D>("Gold");
            hafnium = Content.Load<Texture2D>("Hafnium");
            hassium = Content.Load<Texture2D>("Hassium");
            iridium = Content.Load<Texture2D>("Iridium");
            iron = Content.Load<Texture2D>("Iron");
            lawrencium = Content.Load<Texture2D>("Lawrencium");
            lutetium = Content.Load<Texture2D>("Lutetium");
            manganese = Content.Load<Texture2D>("Manganese");
            meitnerium = Content.Load<Texture2D>("Meitnerium");
            mercury = Content.Load<Texture2D>("Mercury");
            molybdenum = Content.Load<Texture2D>("Molybdenum");
            nickel = Content.Load<Texture2D>("Nickel");
            niobium = Content.Load<Texture2D>("Niobium");
            osmium = Content.Load<Texture2D>("Osmium");
            palladium = Content.Load<Texture2D>("Palladium");
            platinum = Content.Load<Texture2D>("Platinum");
            rhenium = Content.Load<Texture2D>("Rhenium");
            rhodium = Content.Load<Texture2D>("Rhodium");
            ruthemium = Content.Load<Texture2D>("Ruthemium");
            rutherfordium = Content.Load<Texture2D>("Rutherfordium");
            scandium = Content.Load<Texture2D>("Scandium");
            seaborgium = Content.Load<Texture2D>("Seaborgium");
            silver = Content.Load<Texture2D>("Silver");
            tantalum = Content.Load<Texture2D>("Tantalum");
            technetium = Content.Load<Texture2D>("Technetium");
            titanium = Content.Load<Texture2D>("Titanium");
            tungsten = Content.Load<Texture2D>("Tungsten");
            ununnilium = Content.Load<Texture2D>("Ununnilium");
            unununium = Content.Load<Texture2D>("Unununium");
            vanadium = Content.Load<Texture2D>("Vanadium");
            yttrium = Content.Load<Texture2D>("Yttrium");
            zinc = Content.Load<Texture2D>("Zinc");
            zirconium = Content.Load<Texture2D>("Zirconium");

            barium = Content.Load<Texture2D>("Barium");
            beryllium = Content.Load<Texture2D>("Beryllium");
            calcium = Content.Load<Texture2D>("Calcium");
            magnesium = Content.Load<Texture2D>("Magnesium");
            radium = Content.Load<Texture2D>("Radium");
            strontium = Content.Load<Texture2D>("Strontium");

            cesium = Content.Load<Texture2D>("Cesium");
            francium = Content.Load<Texture2D>("Francium");
            lithium = Content.Load<Texture2D>("Lithium");
            potassium = Content.Load<Texture2D>("Potassium");
            rubidium = Content.Load<Texture2D>("Rubidium");
            sodium = Content.Load<Texture2D>("Sodium");

            CActinium = Content.Load<SoundEffect>("CActinium"); 
            CAmericium = Content.Load<SoundEffect>("CAmericium"); 
            CBerkelium = Content.Load<SoundEffect>("CBerkelium"); 
            CCalifornium = Content.Load<SoundEffect>("CCalifornium"); 
            CCurium = Content.Load<SoundEffect>("CCurium"); 
            CEinsteinium = Content.Load<SoundEffect>("CEinsteinium"); 
            CFermium = Content.Load<SoundEffect>("CFermium"); 
            CMendelevium = Content.Load<SoundEffect>("CMendelevium");
            CNeptunium = Content.Load<SoundEffect>("CNeptunium"); 
            CNobelium = Content.Load<SoundEffect>("CNobelium"); 
            CPlutonium = Content.Load<SoundEffect>("CPlutonium"); 
            CProtactinium = Content.Load<SoundEffect>("CProtactinium"); 
            CThorium = Content.Load<SoundEffect>("CThorium"); 
            CUranium = Content.Load<SoundEffect>("CUranium");
            
            JActinium = Content.Load<SoundEffect>("JActinium"); 
            JAmericium = Content.Load<SoundEffect>("JAmericium"); 
            JBerkelium = Content.Load<SoundEffect>("JBerkelium"); 
            JCalifornium = Content.Load<SoundEffect>("JCalifornium");
            JCurium = Content.Load<SoundEffect>("JCurium");
            JEinsteinium = Content.Load<SoundEffect>("JEinsteinium");
            JFermium = Content.Load<SoundEffect>("JFermium");
            JMendelevium = Content.Load<SoundEffect>("JMendelevium");
            JNeptunium = Content.Load<SoundEffect>("JNeptunium");
            JNobelium = Content.Load<SoundEffect>("JNobelium");
            JPlutonium = Content.Load<SoundEffect>("JPlutonium");
            JProtactinium = Content.Load<SoundEffect>("JProtactinium");
            JThorium = Content.Load<SoundEffect>("JThorium");
            JUranium = Content.Load<SoundEffect>("JUranium");

            RActinium = Content.Load<SoundEffect>("RActinium");
            RAmericium = Content.Load<SoundEffect>("RAmericium");
            RBerkelium = Content.Load<SoundEffect>("RBerkelium");
            RCalifornium = Content.Load<SoundEffect>("RCalifornium");
            RCurium = Content.Load<SoundEffect>("RCurium");
            REinsteinium = Content.Load<SoundEffect>("REinsteinium");
            RFermium = Content.Load<SoundEffect>("RFermium");
            RMendelevium = Content.Load<SoundEffect>("RMendelevium");
            RNeptunium = Content.Load<SoundEffect>("RNeptunium");
            RNobelium = Content.Load<SoundEffect>("RNobelium");
            RPlutonium = Content.Load<SoundEffect>("RPlutonium");
            RProtactinium = Content.Load<SoundEffect>("RProtactinium");
            RThorium = Content.Load<SoundEffect>("RThorium");
            RUranium = Content.Load<SoundEffect>("RUranium");

            FontGameOver = Content.Load<SpriteFont>("GameOverScreen");
            FontGameOverNavigation = Content.Load<SpriteFont>("GameOverNavigation");
            DisplayedWordFont = Content.Load<SpriteFont>("DisplayedWord");
            TypeCheckError = Content.Load<SpriteFont>("TypeCheckError");
            LanguageFont = Content.Load<SpriteFont>("Language");

            //Loading and creating all the user interface buttons
            THealth = Content.Load<Texture2D>("Health");
            IsMouseVisible = true;
            btnPlay = new xButton(Content.Load<Texture2D>("PlayButton"), graphics.GraphicsDevice);
            btnPlay.setPosition(new Vector2(500, 700));

            btnActinides = new xButton(Content.Load<Texture2D>("Actinides"), graphics.GraphicsDevice);
            btnActinides.setPosition(new Vector2(5, 300));
            btnAlkalineEarthMetals = new xButton(Content.Load<Texture2D>("Alkaline Earth Metals"), graphics.GraphicsDevice);
            btnAlkalineEarthMetals.setPosition(new Vector2(210, 300));

            btnAlkalaiMetals = new xButton(Content.Load<Texture2D>("Alkalai Metals"), graphics.GraphicsDevice);
            btnAlkalaiMetals.setPosition(new Vector2(995, 300));
            btnPostTransitionMetals = new xButton(Content.Load<Texture2D>("Post-Transition Metals"), graphics.GraphicsDevice);
            btnPostTransitionMetals.setPosition(new Vector2(790, 300));
            
            btnHalogens = new xButton(Content.Load<Texture2D>("Halogens"), graphics.GraphicsDevice);
            btnHalogens.setPosition(new Vector2(5, 435));
            btnLanthanides = new xButton(Content.Load<Texture2D>("Lanthanides"), graphics.GraphicsDevice);
            btnLanthanides.setPosition(new Vector2(210, 435));

            btnMetalloids = new xButton(Content.Load<Texture2D>("Metalloids"), graphics.GraphicsDevice);
            btnMetalloids.setPosition(new Vector2(995, 435));
            btnNonMetals = new xButton(Content.Load<Texture2D>("Non-Metals"), graphics.GraphicsDevice);
            btnNonMetals.setPosition(new Vector2(790, 435));
            
            btnTransitionMetals = new xButton(Content.Load<Texture2D>("Transition Metals"), graphics.GraphicsDevice);
            btnTransitionMetals.setPosition(new Vector2(5, 570));
            btnNobleGases = new xButton(Content.Load<Texture2D>("Noble gases"), graphics.GraphicsDevice);
            btnNobleGases.setPosition(new Vector2(995, 570));

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>

        public void PlayGame()
        {
            Shot.LetterShooting(OldKeys, MyKeys, PlayerStation);
            BackgroundTransitioning();
            Enemies.front.MoveEnemy();
            if (Gameover == false){Enemy.MoveEnemy();}
            
            Word1TexturePositionX = Enemies.front.Spaceship.X - 10;
            Word1TexturePositionY = Enemies.front.Spaceship.Y + 95;    //Code that ensures the word is attatched and following the enemy
            Word2TexturePositionX = Enemies.front.Spaceship.X - 10;
            Word2TexturePositionY = Enemies.front.Spaceship.Y + 95;

            PlayerStation.SpaceStationGun = new Rectangle((int)PlayerStation.spritePosition.X, (int)PlayerStation.spritePosition.Y, SpriteTexture.Width, SpriteTexture.Height);
            PlayerStation.SpriteOrigin = new Vector2(SpriteTexture.Width / 2, SpriteTexture.Height / 2); //Player station gun

            MouseState mouse = Mouse.GetState();
            IsMouseVisible = true;

            PlayerStation.distance.X = PlayerStation.spritePosition.X - mouse.X;
            PlayerStation.distance.Y = PlayerStation.spritePosition.Y - mouse.Y;
            PlayerStation.Rotate(); //rotation code

            if (LockedWord == false)
            {
                OldWord = Enemies.front.Word; //Code to play a sound whenevera new element appears
                SoundPlayed = false;
                LockedWord = true;
            }
            

            if (Shot.UseBullet().Intersects(Enemies.front.Spaceship)) //Bullet intersecrtion code, if a shot hits the enemy ship
            {

                Shot.HideShotOffScreen();


                LetterPosition = Enemies.front.Word.IndexOf(Shot.ReturnCurrentChar());
                if (Enemies.front.Word.Contains(Shot.ReturnCurrentChar()) == true && LetterPosition == positionpointer) //Word adjustment code
                {
                    Score = Score + 10;
                    positionpointer++;
                    if (LetterPosition < Enemies.front.Word.Length)
                    {
                        Enemies.front.Word = ReplaceAtIndex(LetterPosition, '_', Enemies.front.Word);
                        Shot.ResetCurretChar();
                    }
                }
                else
                {
                    PlayerStation.DecrementPlayerHealth();
                }
            }


            PlayerStation.GetPlayerHealth();

            if (PlayerStation.GetPlayerHealth() == 0)
            {
                Gameover = true;
            }

            if (MyKeys.IsKeyDown(Keys.C) && Victory == true)
            {
                gameState = TGameState.splashScreen;
                Victory = false;
            }

            if (Enemies.front.Word.All(c => c == '_')) //If the entirety of the word has been correctly spelt out, load the next word in the list
            {
                positionpointer = 0;
                Enemies.Remove(out items);
                isTimerOn = true;
                LockedWord = false;
            }

            if (Enemies.IsEmpty() || (Gameover == true))
            {
                if (Enemies.IsEmpty())
                { 
                    Victory = true;
                    Enemies.Add(99, "Congratulations");
                }
                else { Victory = false; }
            }
        }
        
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            OldKeys = MyKeys;
            MyKeys = Keyboard.GetState();
            MouseState mouse = Mouse.GetState();

            switch (gameState)
            {
                case TGameState.splashScreen:

                    if (btnPlay.isClicked == true) gameState = TGameState.playGame;
                    btnPlay.Update(mouse);

                    if (btnActinides.isClicked == true)         //Code for all the possible game states that a player gets through before the game
                    {
                        ElementChoice = "Actinides";
                        gameState = TGameState.Rules;
                    }
                    btnActinides.Update(mouse);

                    if (btnAlkalaiMetals.isClicked == true)
                    {
                        ElementChoice = "AlkalaiMetals";
                        gameState = TGameState.Rules; 
                    }
                    btnAlkalaiMetals.Update(mouse);

                    if (btnAlkalineEarthMetals.isClicked == true)
                    {
                        ElementChoice = "AkalineEarthMetals";
                        gameState = TGameState.Rules;
                    }
                    btnAlkalineEarthMetals.Update(mouse);

                    if (btnHalogens.isClicked == true)
                    {
                        ElementChoice = "Halogens";
                        gameState = TGameState.Rules;
                    }
                    btnHalogens.Update(mouse);

                    if (btnLanthanides.isClicked == true)
                    {
                        ElementChoice = "Lanthanides";
                        gameState = TGameState.Rules;
                    }
                    btnLanthanides.Update(mouse);

                    if (btnMetalloids.isClicked == true)
                    {
                        ElementChoice = "Metalloids";
                        gameState = TGameState.Rules;  
                    }
                    btnMetalloids.Update(mouse);


                    if (btnNobleGases.isClicked == true)
                    {
                        ElementChoice = "NobleGases";
                        gameState = TGameState.Rules; 
                    }
                    btnNobleGases.Update(mouse);

                    if (btnNonMetals.isClicked == true)
                    {
                        ElementChoice = "NonMetals";
                        gameState = TGameState.Rules;   
                    }
                    btnNonMetals.Update(mouse);

                    if (btnPostTransitionMetals.isClicked == true)
                    {
                        ElementChoice = "PostTransitionMetals";
                        gameState = TGameState.Rules;  
                    }
                    btnPostTransitionMetals.Update(mouse);

                    if (btnTransitionMetals.isClicked == true)
                    {
                        ElementChoice = "TransitionMetals";
                        gameState = TGameState.Rules; 
                    }
                    btnTransitionMetals.Update(mouse);
                        
                    break;

                case TGameState.Rules:
                    if(MyKeys.IsKeyDown(Keys.D1) && (OldKeys.IsKeyUp(Keys.D1)))  //Language choice code
                    { AssistLanguageChosen = "Russian"; }
                    if (MyKeys.IsKeyDown(Keys.D2) && (OldKeys.IsKeyUp(Keys.D2)))
                    { AssistLanguageChosen = "Chinese"; }
                    if (MyKeys.IsKeyDown(Keys.D3) && (OldKeys.IsKeyUp(Keys.D3)))
                    { AssistLanguageChosen = "Japanese"; }
                    
                    if (MyKeys.IsKeyDown(Keys.Enter) && (OldKeys.IsKeyUp(Keys.Enter)))
                    {
                        gameState = TGameState.Revision;
                    }
                    break;

                case TGameState.Revision:
                    if (MyKeys.IsKeyDown(Keys.Enter) && (OldKeys.IsKeyUp(Keys.Enter)))
                    {
                        gameState = TGameState.playGame;
                        positionpointer = 0;
                        Initialize();
                    }
                    break;

                case TGameState.playGame:

                    PlayGame();

                    if (Gameover == true)
                    {
                        gameState = TGameState.gameOver;
                    }
                    break;

                case TGameState.gameOver:

                    if (MyKeys.IsKeyDown(Keys.C))
                    {
                        PlayerStation.ResetHealth();
                        LockedWord = false;
                        gameState = TGameState.splashScreen;
                    }
                    break;
            }

            base.Update(gameTime);
        }

        static string ReplaceAtIndex(int Position, char NewChar, string word) //Replacing the appropriate letter, with the character that was shot code
        {
            char[] letters = word.ToCharArray();
            letters[Position] = NewChar;
            return string.Join("", letters);
        }

        public void MyDrawGame()
        {
            spriteBatch.Draw(TBackground, Background, Color.White);
            spriteBatch.Draw(TBackground2, Background2, Color.White);
            spriteBatch.Draw(TSpacestation, Spacestation, Color.White);
            spriteBatch.Draw(TShot, Shot.UseBullet(), Color.White);
            spriteBatch.Draw(SpriteTexture, PlayerStation.spritePosition, null, Color.White, PlayerStation.GetRotation(), PlayerStation.SpriteOrigin, 1f, SpriteEffects.None, 0);

            spriteBatch.DrawString(DisplayedWordFont, Enemies.front.Word, new Vector2(Word1TexturePositionX, Word1TexturePositionY), Color.White);
            spriteBatch.DrawString(FontGameOverNavigation, Convert.ToString(Score), new Vector2(SCREEN_WIDTH - 100, 0), Color.WhiteSmoke);
            spriteBatch.DrawString(FontGameOverNavigation, Convert.ToString(PlayerStation.GetPlayerHealth()), new Vector2(SCREEN_WIDTH - 1175, 0), Color.Black);
            spriteBatch.Draw(THealth, HealthIcon, Color.White);

            //Drawing code, to ensure that the correct 2D texture is being updatedly drawn
                switch (ElementChoice)
                {
                    case "Actinides":
                        switch (OldWord)
                        {
                            case "ACTINIUM":
                                spriteBatch.Draw(actinium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RActinium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CActinium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JActinium.Play();
                                        SoundPlayed = true;
                                    }
                                    
                                }
                                break;

                            case "AMERICIUM":
                                spriteBatch.Draw(americium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RAmericium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CAmericium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JAmericium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "BERKELIUM":
                                spriteBatch.Draw(berkelium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RBerkelium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CBerkelium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JBerkelium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "CALIFORNIUM":
                                spriteBatch.Draw(californium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RCalifornium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CCalifornium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JCalifornium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "CURIUM":
                                spriteBatch.Draw(curium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RCurium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CCurium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JCurium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "EINSTEINIUM":
                                spriteBatch.Draw(einsteinium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        REinsteinium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CEinsteinium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JEinsteinium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "FERMIUM":
                                spriteBatch.Draw(fermium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RFermium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CFermium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JFermium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "MENDELEVIUM":
                                spriteBatch.Draw(mendelevium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RMendelevium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CMendelevium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JMendelevium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "NEPTUNIUM":
                                spriteBatch.Draw(neptunium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RNeptunium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CNeptunium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JNeptunium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "NOBELIUM":
                                spriteBatch.Draw(nobelium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RNobelium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CNobelium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JNobelium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "PLUTONIUM":
                                spriteBatch.Draw(plutonium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RPlutonium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CPlutonium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JPlutonium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "PROTACTINIUM":
                                spriteBatch.Draw(protactinium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RProtactinium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CProtactinium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JProtactinium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "THORIUM":
                                spriteBatch.Draw(thorium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RThorium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CThorium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JThorium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;

                            case "URANIUM":
                                spriteBatch.Draw(uranium, Enemies.front.Spaceship, Color.White);
                                if (SoundPlayed == false)
                                {
                                    if (AssistLanguageChosen == "Russian")
                                    {
                                        RUranium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Chinese")
                                    {
                                        CUranium.Play();
                                        SoundPlayed = true;
                                    }
                                    else if (AssistLanguageChosen == "Japanese")
                                    {
                                        JUranium.Play();
                                        SoundPlayed = true;
                                    }

                                }
                                break;
                        }

                        break;

                    case "AlkalaiMetals":
                        switch (OldWord)
                        {
                            case "CESIUM":
                                spriteBatch.Draw(cesium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "FRANCIUM":
                                spriteBatch.Draw(francium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "LITHIUM":
                                spriteBatch.Draw(lithium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "POTASSIUM":
                                spriteBatch.Draw(potassium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RUBIDIUM":
                                spriteBatch.Draw(rubidium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SODIUM":
                                spriteBatch.Draw(sodium, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "AlkalineEarthMetals":
                        switch (OldWord)
                        {
                            case "BARIUM":
                                spriteBatch.Draw(barium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "BERYLLIUM":
                                spriteBatch.Draw(beryllium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "CALCIUM":
                                spriteBatch.Draw(calcium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "MAGNESIUM":
                                spriteBatch.Draw(magnesium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RADIUM":
                                spriteBatch.Draw(radium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "STRONTIUM":
                                spriteBatch.Draw(strontium, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "Halogens":
                        switch (OldWord)
                        {
                            case "ASTATINE":
                                spriteBatch.Draw(astatine, Enemies.front.Spaceship, Color.White);
                                break;

                            case "BROMINE":
                                spriteBatch.Draw(bromine, Enemies.front.Spaceship, Color.White);
                                break;

                            case "LITHIUM":
                                spriteBatch.Draw(lithium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "FLUORINE":
                                spriteBatch.Draw(fluorine, Enemies.front.Spaceship, Color.White);
                                break;

                            case "IODINE":
                                spriteBatch.Draw(iodine, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "Lanthanides":
                        switch (OldWord)
                        {
                            case "CERIUM":
                                spriteBatch.Draw(cerium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "DYSPROSIUM":
                                spriteBatch.Draw(dysprosium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "ERBIUM":
                                spriteBatch.Draw(erbium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "FLUORINE":
                                spriteBatch.Draw(fluorine, Enemies.front.Spaceship, Color.White);
                                break;

                            case "EUROPIUM":
                                spriteBatch.Draw(europium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "GADOLINIUM":
                                spriteBatch.Draw(gadolinium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "HOLMIUM":
                                spriteBatch.Draw(holmium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "LANTHANUM":
                                spriteBatch.Draw(lanthanum, Enemies.front.Spaceship, Color.White);
                                break;

                            case "NEODYMIUM":
                                spriteBatch.Draw(neodymium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "PRASEODYMIUM":
                                spriteBatch.Draw(praseodymium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "PROMETHIUM":
                                spriteBatch.Draw(promethium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SAMARIUM":
                                spriteBatch.Draw(samarium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TERBIUM":
                                spriteBatch.Draw(terbium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "THULIUM":
                                spriteBatch.Draw(thulium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "YTTERBIUM":
                                spriteBatch.Draw(ytterbium, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "Metalloids":
                        switch (OldWord)
                        {
                            case "ANTIMONY":
                                spriteBatch.Draw(antimony, Enemies.front.Spaceship, Color.White);
                                break;

                            case "ARSENIC":
                                spriteBatch.Draw(arsenic, Enemies.front.Spaceship, Color.White);
                                break;

                            case "BORON":
                                spriteBatch.Draw(boron, Enemies.front.Spaceship, Color.White);
                                break;

                            case "GERMANIUM":
                                spriteBatch.Draw(germanium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SILICON":
                                spriteBatch.Draw(silicon, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TELLURIUM":
                                spriteBatch.Draw(tellurium, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "NobleGases":
                        switch (OldWord)
                        {
                            case "ARGON":
                                spriteBatch.Draw(argon, Enemies.front.Spaceship, Color.White);
                                break;

                            case "HELIUM":
                                spriteBatch.Draw(helium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "KRYPTON":
                                spriteBatch.Draw(krypton, Enemies.front.Spaceship, Color.White);
                                break;

                            case "NEON":
                                spriteBatch.Draw(neon, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RADON":
                                spriteBatch.Draw(radon, Enemies.front.Spaceship, Color.White);
                                break;

                            case "XENON":
                                spriteBatch.Draw(xenon, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "NonMetals":
                        switch (OldWord)
                        {
                            case "CARBON":
                                spriteBatch.Draw(carbon, Enemies.front.Spaceship, Color.White);
                                break;

                            case "HYDROGEN":
                                spriteBatch.Draw(hydrogen, Enemies.front.Spaceship, Color.White);
                                break;

                            case "NITROGEN":
                                spriteBatch.Draw(nitrogen, Enemies.front.Spaceship, Color.White);
                                break;

                            case "OXYGEN":
                                spriteBatch.Draw(oxygen, Enemies.front.Spaceship, Color.White);
                                break;

                            case "PHOSPHORUS":
                                spriteBatch.Draw(phosphorus, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SELENIUM":
                                spriteBatch.Draw(selenium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SULFUR":
                                spriteBatch.Draw(sulfur, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "PostTransitionMetals":
                        switch (OldWord)
                        {
                            case "ALUMINIUM":
                                spriteBatch.Draw(aluminium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "BISMUTH":
                                spriteBatch.Draw(bismuth, Enemies.front.Spaceship, Color.White);
                                break;

                            case "GALLIUM":
                                spriteBatch.Draw(gallium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "INDIUM":
                                spriteBatch.Draw(indium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "LEAD":
                                spriteBatch.Draw(lead, Enemies.front.Spaceship, Color.White);
                                break;

                            case "POLONIUM":
                                spriteBatch.Draw(polonium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "THALLIUM":
                                spriteBatch.Draw(thallium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TIN":
                                spriteBatch.Draw(tin, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;

                    case "TransitionMetals":
                        switch (OldWord)
                        {
                            case "BOHRIUM":
                                spriteBatch.Draw(bohrium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "CADMIUM":
                                spriteBatch.Draw(cadmium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "CHROMIUM":
                                spriteBatch.Draw(chromium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "COBALT":
                                spriteBatch.Draw(cobalt, Enemies.front.Spaceship, Color.White);
                                break;

                            case "COPPER":
                                spriteBatch.Draw(copper, Enemies.front.Spaceship, Color.White);
                                break;

                            case "DUBNIUM":
                                spriteBatch.Draw(dubnium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "GOLD":
                                spriteBatch.Draw(gold, Enemies.front.Spaceship, Color.White);
                                break;

                            case "HAFNIUM":
                                spriteBatch.Draw(hafnium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "HASSIUM":
                                spriteBatch.Draw(hassium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "IRIDIUM":
                                spriteBatch.Draw(iridium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "IRON":
                                spriteBatch.Draw(iron, Enemies.front.Spaceship, Color.White);
                                break;

                            case "LAWRENCIUM":
                                spriteBatch.Draw(lawrencium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "LUTETIUM":
                                spriteBatch.Draw(lutetium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "MANGANESE":
                                spriteBatch.Draw(manganese, Enemies.front.Spaceship, Color.White);
                                break;

                            case "MEITNERIUM":
                                spriteBatch.Draw(meitnerium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "MERCURY":
                                spriteBatch.Draw(mercury, Enemies.front.Spaceship, Color.White);
                                break;

                            case "MOLYBDENUM":
                                spriteBatch.Draw(molybdenum, Enemies.front.Spaceship, Color.White);
                                break;

                            case "NICKEL":
                                spriteBatch.Draw(nickel, Enemies.front.Spaceship, Color.White);
                                break;

                            case "NIOBIUM":
                                spriteBatch.Draw(niobium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "OSMIUM":
                                spriteBatch.Draw(osmium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "PALLADIUM":
                                spriteBatch.Draw(palladium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "PLATINUM":
                                spriteBatch.Draw(platinum, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RHENIUM":
                                spriteBatch.Draw(rhenium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RHODIUM":
                                spriteBatch.Draw(rhodium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RUTHEMIUM":
                                spriteBatch.Draw(ruthemium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "RUTHERFORDIUM":
                                spriteBatch.Draw(rutherfordium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SCANDIUM":
                                spriteBatch.Draw(scandium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SEABORGIUM":
                                spriteBatch.Draw(seaborgium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "SILVER":
                                spriteBatch.Draw(silver, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TANTALUM":
                                spriteBatch.Draw(tantalum, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TECHNETIUM":
                                spriteBatch.Draw(technetium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TITANIUM":
                                spriteBatch.Draw(titanium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "TUNGSTEN":
                                spriteBatch.Draw(tungsten, Enemies.front.Spaceship, Color.White);
                                break;

                            case "UNUNNILIUM":
                                spriteBatch.Draw(ununnilium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "UNUNUNIUM":
                                spriteBatch.Draw(unununium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "VANADIUM":
                                spriteBatch.Draw(vanadium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "YTTRIUM":
                                spriteBatch.Draw(yttrium, Enemies.front.Spaceship, Color.White);
                                break;

                            case "ZINC":
                                spriteBatch.Draw(zinc, Enemies.front.Spaceship, Color.White);
                                break;

                            case "ZIRCONIUM":
                                spriteBatch.Draw(zirconium, Enemies.front.Spaceship, Color.White);
                                break;
                        }
                        break;
                }
          

            if (Victory == true)
            {
                spriteBatch.DrawString(FontGameOverNavigation,
                ROUND_COMPLETED_PROMPT, new Vector2(SCREEN_WIDTH / 5,
                SCREEN_HEIGHT / 3), Color.LawnGreen);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();
            switch (gameState)
            {
                case TGameState.splashScreen:
                    spriteBatch.Draw(TsplashScreen, MenuScreen, Color.White);
                    //btnPlay.Draw(spriteBatch);
                    btnActinides.Draw(spriteBatch);
                    btnAlkalaiMetals.Draw(spriteBatch);
                    btnAlkalineEarthMetals.Draw(spriteBatch);
                    btnHalogens.Draw(spriteBatch);
                    btnLanthanides.Draw(spriteBatch);
                    btnMetalloids.Draw(spriteBatch);
                    btnNobleGases.Draw(spriteBatch);
                    btnNonMetals.Draw(spriteBatch);
                    btnPostTransitionMetals.Draw(spriteBatch);
                    btnTransitionMetals.Draw(spriteBatch);

                    break;

                case TGameState.Rules:
                    spriteBatch.Draw(TRules, MenuScreen, Color.White);
                    spriteBatch.DrawString(LanguageFont, "Select a language for pronounciation assistance; \n [1] Russian \n [2] Chinese \n [3] Japanese", new Vector2(SCREEN_WIDTH - 1150, 700), Color.Yellow);
                    spriteBatch.DrawString(LanguageFont, "Chosen: " + AssistLanguageChosen, new Vector2(SCREEN_WIDTH - 500, 800), Color.Green);
                    break;

                case TGameState.Revision:
                    spriteBatch.Draw(TPeriodic, MenuScreen, Color.White);
                    spriteBatch.DrawString(LanguageFont, "Quick revision of the " + AssistLanguageChosen + "Periodic Table", new Vector2(SCREEN_WIDTH - 1050, 100), Color.Orange);
                    spriteBatch.DrawString(LanguageFont, "Press ENTER to start", new Vector2(SCREEN_WIDTH - 800, 200), Color.WhiteSmoke);
                    if (AssistLanguageChosen == "Russian")
                    {
                        spriteBatch.Draw(RussianPeriodicTable, PeriodicTable, Color.White);
                    }
                    else if (AssistLanguageChosen == "Chinese")
                    {
                        spriteBatch.Draw(ChinesePeriodicTable, PeriodicTable, Color.White);
                    }
                    else if (AssistLanguageChosen == "Japanese")
                    {
                        spriteBatch.Draw(JapanesePeriodicTable, PeriodicTable, Color.White);
                    }
                    break;

                case TGameState.playGame:
                    MyDrawGame();
                    break;

                case TGameState.gameOver:
                    MyDrawGame();
                    spriteBatch.DrawString(FontGameOver, GAME_OVER_PROMPT, new Vector2(SCREEN_WIDTH / 4, 200), Color.Red);
                    spriteBatch.DrawString(FontGameOverNavigation, GAME_OVER_NAVIGATION, new Vector2(100, 800), Color.White);
                    break;
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void BackgroundTransitioning() //Transitioning background
        {
            Background.Y += SPEED;
            Background2.Y += SPEED;

            if (Background.Y >= SCREEN_HEIGHT)
            {
                Background.Y = Background2.Y - SCREEN_HEIGHT;
            }
            else if (Background2.Y >= SCREEN_HEIGHT)
            {
                Background2.Y = Background.Y - SCREEN_HEIGHT;
            }
        }
    }

    class TShot
 {
        private Vector2 ShotPosition;
        private Rectangle Shot;
        private bool Fired;
        public int SHOT_WIDTH = 30;
        public int SHOT_HEIGHT = 30;
        public int SHOT_SPEED = 20;
        private string CurrentChar; //The character that is determined by which key is currently being pressed

        public TShot()
        {
            Shot = new Rectangle(0 - TEnemy.SPACESHIP_WIDTH, 0 - TEnemy.SPACESHIP_HEIGHT, SHOT_WIDTH, SHOT_HEIGHT);
            ShotPosition = new Vector2(Shot.Width / 2, Shot.Width / 2);
        }

        public Rectangle UseBullet()
        {
            return Shot;
        }

        public void SetShotFired(int RectangleNumber, bool HasBeenFired)
        {
            Fired = HasBeenFired;
        }

        public bool HasShotBeenFired(int RectangleNumber)
        {
            if (Fired == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string ReturnCurrentChar()
        {
            return CurrentChar;
        }
        
        public void ResetCurretChar()
        {
            CurrentChar = "";
        }
        
        public void HideShotOffScreen()
        {
            Shot.Y = 0 - SHOT_HEIGHT;
            Shot.X = 0 - SHOT_WIDTH;
        }

        public string LetterShooting(KeyboardState OldKeys, KeyboardState MyKeys, SpaceStationControl PlayerStation) //If statements for each of the letter shots
        {
            if (Fired == true)
            {
                Shot.Y -= (int)PlayerStation.distance.Y / 6;
                Shot.X -= (int)PlayerStation.distance.X / 6;
                if (Shot.Y < 0 - SHOT_HEIGHT)
                {
                    Fired = false;
                }
            }
            
            if (MyKeys.IsKeyDown(Keys.A) && OldKeys.IsKeyUp(Keys.A) && Fired == false)
            {
                CurrentChar = "A";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.B) && OldKeys.IsKeyUp(Keys.B) && Fired == false)
            {
                CurrentChar = "B";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.C) && OldKeys.IsKeyUp(Keys.C) && Fired == false)
            {
                CurrentChar = "C";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.D) && OldKeys.IsKeyUp(Keys.D) && Fired == false)
            {
                CurrentChar = "D";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.E) && OldKeys.IsKeyUp(Keys.E) && Fired == false)
            {
                CurrentChar = "E";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.Q) && OldKeys.IsKeyUp(Keys.Q) && Fired == false)
            {
                CurrentChar = "Q";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.W) && OldKeys.IsKeyUp(Keys.W) && Fired == false)
            {
                CurrentChar = "W";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.R) && OldKeys.IsKeyUp(Keys.R) && Fired == false)
            {
                CurrentChar = "R";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.T) && OldKeys.IsKeyUp(Keys.T) && Fired == false)
            {
                CurrentChar = "T";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.Y) && OldKeys.IsKeyUp(Keys.Y) && Fired == false)
            {
                CurrentChar = "Y";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.U) && OldKeys.IsKeyUp(Keys.U) && Fired == false)
            {
                CurrentChar = "U";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.I) && OldKeys.IsKeyUp(Keys.I) && Fired == false)
            {
                CurrentChar = "I";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.O) && OldKeys.IsKeyUp(Keys.O) && Fired == false)
            {
                CurrentChar = "O";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.P) && OldKeys.IsKeyUp(Keys.P) && Fired == false)
            {
                CurrentChar = "P";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.S) && OldKeys.IsKeyUp(Keys.S) && Fired == false)
            {
                CurrentChar = "S";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.F) && OldKeys.IsKeyUp(Keys.F) && Fired == false)
            {
                CurrentChar = "F";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.G) && OldKeys.IsKeyUp(Keys.G) && Fired == false)
            {
                CurrentChar = "G";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.H) && OldKeys.IsKeyUp(Keys.H) && Fired == false)
            {
                CurrentChar = "H";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.J) && OldKeys.IsKeyUp(Keys.J) && Fired == false)
            {
                CurrentChar = "J";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.K) && OldKeys.IsKeyUp(Keys.K) && Fired == false)
            {
                CurrentChar = "K";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.L) && OldKeys.IsKeyUp(Keys.L) && Fired == false)
            {
                CurrentChar = "L";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.Z) && OldKeys.IsKeyUp(Keys.Z) && Fired == false)
            {
                CurrentChar = "Z";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.X) && OldKeys.IsKeyUp(Keys.X) && Fired == false)
            {
                CurrentChar = "X";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.V) && OldKeys.IsKeyUp(Keys.V) && Fired == false)
            {
                CurrentChar = "V";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.N) && OldKeys.IsKeyUp(Keys.N) && Fired == false)
            {
                CurrentChar = "N";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }

            if (MyKeys.IsKeyDown(Keys.M) && OldKeys.IsKeyUp(Keys.M) && Fired == false)
            {
                CurrentChar = "M";
                Shot.X = PlayerStation.SpaceStationGun.X;
                Shot.Y = PlayerStation.SpaceStationGun.Y;
                Fired = true;
            }
            return CurrentChar;
        }
    }

    class SpaceStationControl
    {
        public Rectangle SpaceStationGun;
        public const int SPACESTATION_HEIGHT = 300;
        public Vector2 SpriteOrigin;
        public Vector2 distance;
        public Vector2 spritePosition;
        private float rotation;
        private int PlayerHealth;


        public SpaceStationControl()
        {
            PlayerHealth = 3;
        }

        public int GetSpaceStationHeight()
        {
            return SPACESTATION_HEIGHT;
        }

        public void Rotate()
        {
            rotation = (float)Math.Atan2(distance.Y, distance.X);
        }

        public float GetRotation()
        {
            return rotation;
        }

        public void DecrementPlayerHealth()
        {
            PlayerHealth--;
        }

        public void ResetHealth()
        {
            PlayerHealth = 3;
        }

        public int GetPlayerHealth()
        {
            return PlayerHealth;
        }

    }

     class TEnemy
     {
        public TEnemy pointer;
        public const int SCREEN_WIDTH = 1200;
        public const int SPACESHIP_WIDTH = 100;
        public const int SPACESHIP_HEIGHT = 100;
        public Rectangle Spaceship;
        private string Difficulty;
        private Vector2 EnemyVelocity;
        private int EnemyID;
        public string Word;

        public TEnemy(int IDPassed, string WordPassed)
        {
            EnemyID = IDPassed;
            Word = WordPassed.ToUpper();
            pointer = null;

            Difficulty = "Easy";
            if (Difficulty == "Hard")
            {
                EnemyVelocity.X = 10;
                EnemyVelocity.Y = 10;
            }
            else if (Difficulty == "Medium")
            {
                EnemyVelocity.X = 6;
                EnemyVelocity.Y = 6;
            }
            else
            {
                EnemyVelocity.X = 3;
                EnemyVelocity.Y = 3;
            }

            Spaceship = new Rectangle();
            Spaceship = new Rectangle(300, 200, SPACESHIP_WIDTH, SPACESHIP_HEIGHT);

        }


        public void MoveEnemy()
        {
            Spaceship.X = Spaceship.X + (int)EnemyVelocity.X;
            Spaceship.Y = Spaceship.Y + (int)EnemyVelocity.Y;

            if (Spaceship.X <= 0)
                EnemyVelocity.X = -EnemyVelocity.X;
            if (Spaceship.X + SPACESHIP_WIDTH >= SCREEN_WIDTH)
                EnemyVelocity.X = -EnemyVelocity.X;

            if (Spaceship.Y <= 0)
                EnemyVelocity.Y = -EnemyVelocity.Y;
            if (Spaceship.Y + SPACESHIP_HEIGHT >= 500)
                EnemyVelocity.Y = -EnemyVelocity.Y;
        }
        public string GetDifficultyPlayedOn()
        {
            return Difficulty;
        }
    }

    class TDynamicQueue
    {
        private int counter = 0;
        //ListArray for every element group that is populated with the appropriate elements
        List<string> Actinides = new List<string> {"ACTINIUM", "AMERICIUM", "BERKELIUM", "CALIFORNIUM", "CURIUM", "EINSTEINIUM", "FERMIUM", "MENDELEVIUM", "NEPTUNIUM", "NOBELIUM", "PLUTONIUM", "PROTACTINIUM", "THORIUM", "URANIUM"};
        List<string> AlkalaiMetals = new List<string> {"CESIUM", "FRANCIUM", "LITHIUM", "POTASSIUM", "RUBIDIUM", "SODIUM"};
        List<string> AlkalineEarthMetals = new List<string> {"BARIUM", "BERYLLIUM", "CALCIUM", "MAGNESIUM", "RADIUM", "STRONTIUM"};
        List<string> Halogens = new List<string> {"ASTATINE", "BROMINE", "CHLORINE", "FLUORINE", "IODINE"};
        List<string> Lanthanides = new List<string> {"CERIUM", "DYSPROSIUM", "ERBIUM", "EUROPIUM", "GADOLINIUM", "HOLMIUM", "LANTHANUM", "NEODYMIUM", "PRASEODYMIUM", "PROMETHIUM", "SAMARIUM", "TERBIUM", "THULIUM", "YTTERBIUM"};
        List<string> Metalloids = new List<string> {"ANTIMONY", "ARSENIC", "BORON", "GERMANIUM", "SILICON", "TELLURIUM"};
        List<string> NobleGases = new List<string> {"ARGON", "HELIUM", "KRYPTON", "NEON", "RADON", "XENON"};
        List<string> NonMetals = new List<string> {"CARBON", "HYDROGEN", "NITROGEN", "OXYGEN", "PHOSPHORUS", "SELENIUM", "SULFUR"};
        List<string> PostTransitionMetals = new List<string> {"ALUMINIUM", "BISMUTH", "GALLIUM", "INDIUM", "LEAD", "POLONIUM", "THALLIUM", "TIN"};
        List<string> TransitionMetals = new List<string>{"BOHRIUM", "CADMIUM", "CHROMIUM", "COBALT", "COPPER", "DUBNIUM", "GOLD", "HAFNIUM", "HASSIUM", "IRIDIUM", "IRON", "LAWRENCIUM", "LUTETIUM", "MANGANESE", "MEITNERIUM", "MERCURY", "MOLYBDENUM", "NICKEL", "NIOBIUM", "OSMIUM", "PALLADIUM", "PLATINUM", "RHENIUM", "RHODIUM", "RUTHEMIUM", "RUTHERFORDIUM", "SCANDIUM", "SEABORGIUM", "SILVER", "TANTALUM", "TECHNETIUM", "TITANIUM", "TUNGSTEN", "UNUNNILIUM", "UNUNUNIUM", "VANADIUM", "YTTRIUM", "ZINC", "ZIRCONIUM"};

        public TEnemy front, rear;
        private int size;
        string GroupChosen;

        public TDynamicQueue(string Choice)
        {
            size = 0;
            front = null; //point to front item
            rear = null; //points to rear item
            GroupChosen = Choice;

            switch (Choice)
            {
                case "Actinides":
                    while (counter != (Actinides.Count() - 1))
                    {
                        Add(counter, Actinides.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "AlkalaiMetals":
                    while (counter != (AlkalaiMetals.Count() - 1))
                    {
                        Add(counter, AlkalaiMetals.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "AlkalineEarthMetals":
                    while (counter != (AlkalineEarthMetals.Count() - 1))
                    {
                        Add(counter, AlkalineEarthMetals.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "Halogens":
                    while (counter != (Halogens.Count() - 1))
                    {
                        Add(counter, Halogens.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "Lanthanides":
                    while (counter != (Lanthanides.Count() - 1))
                    {
                        Add(counter, Lanthanides.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "Metalloids":
                    while (counter != (Metalloids.Count() - 1))
                    {
                        Add(counter, Metalloids.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "NobleGases":
                    while (counter != (NobleGases.Count() - 1))
                    {
                        Add(counter, NobleGases.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "NonMetals":
                    while (counter != (NonMetals.Count() - 1))
                    {
                        Add(counter, NonMetals.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "PostTransitionMetals":
                    while (counter != (PostTransitionMetals.Count() - 1))
                    {
                        Add(counter, PostTransitionMetals.ElementAt(counter));
                        counter++;
                    }
                    break;

                case "TransitionMetals":
                    while (counter != (TransitionMetals.Count() - 1))
                    {
                        Add(counter, TransitionMetals.ElementAt(counter));
                        counter++;
                    }
                    break;
            }
            
        }
        public int GetSize() { return size; }
        public bool IsFull() { return false; }
        public bool IsEmpty()
        {
            if (front == null)
                return true;
            else
                return false;
        }
        public void Add(int EnemyID, string Word)  //Typical adding and removing subroutines of a dynamic queue
        {
            if (size == 0)
            {
                rear = new TEnemy(EnemyID, Word);
                front = rear;
            }
            else
            {
                rear.pointer = new TEnemy(EnemyID, Word);
                rear = rear.pointer;
            }
            size++;
        }
        public bool Remove(out object item)
        {
            if (IsEmpty())
            {
                item = null;
                return false;
            }
            else
            {
                size--;
                item = front.Word;
                front = front.pointer;
                return true;
            }
        }
    }

    class xButton //Class dedicated to the virtual buttons a user can click on to select their chosen option
    {
        Texture2D texture;
        Vector2 position;
        Rectangle rectangle;

        Color colour = new Color(255, 255, 255, 255);

        public Vector2 size;

        public xButton(Texture2D newTexture, GraphicsDevice graphics)
        {
            texture = newTexture;

            size = new Vector2(graphics.Viewport.Width / 6, graphics.Viewport.Height / 7);
        }

        bool down;
        public bool isClicked;
        public void Update(MouseState mouse)
        {
            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);

            if (mouseRectangle.Intersects(rectangle))
            {
                if (colour.A == 255) down = false;
                if (colour.A == 0) down = true;
                if (down) colour.A += 3; else colour.A -= 3;
                if (mouse.LeftButton == ButtonState.Pressed) isClicked = true;
            }
            else if (colour.A < 255)
            {
                colour.A += 3;
                isClicked = false;
            }
        }

        public void setPosition(Vector2 newPosition)
        {
            position = newPosition;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rectangle, colour);
        }

    }
}
